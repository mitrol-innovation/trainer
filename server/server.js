const express = require('express')
const path = require('path')

const app = express()
const cors = require('cors')

app.use(cors({
    origin: '*',
    methods: '*' // ['GET', 'POST'],
    // credentials:true
}))

app.use(express.json())


console.logger = function (str, args) {
    let a = ''
    if (args) {
        a = args
    }
    const d = new Date()
    d.setTime(d.getTime() - (d.getTimezoneOffset() * 60 * 1000))
    const val = d.toISOString() + ' => ' + str + a + '\r\n'
    console.log(val)
}

app.use('/', express.static(path.join(__dirname, 'public')))

const botRouter = require('./routes/bot')
app.use('/api/bot/', botRouter)

const port = process.env.WEBPORT || 10000
app.listen(port, () => {
    console.logger(`Listening to requests on port: ${port}`)
})

