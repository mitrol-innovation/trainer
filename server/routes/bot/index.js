const express = require('express')
const router = express.Router()
const path = require('path')
const fs = require('fs')

router.route('/info').get((req, res) => {
    data = {
        '/': 'Hi Word test',
        '/info': 'This route'
    }
    res.locals = {
        req: null,
        res: data
    }
    res.json(data)
})

router.route('/file/:type').get((req, res) => {
    let fn = ''
    if (req.params.type === 'kbs') {
        fn = 'nlu.md'
    }
    if (req.params.type === 'bm') {
        fn = 'nlu.md'
    }
    if (req.params.type === 'csv') {
        req.params.type = 'kbs'
        fn = 'bot_responses.csv'
    }
    const x = path.resolve(__dirname, `../../chatbot/worker_${req.params.type}`, fn)
    console.log('get path url:', x)
    res.sendFile(x)
})
router.route('/file').post((req, res) => {
    if (req.body.bm) {
        const x = path.resolve(__dirname, '../../chatbot/worker_bm', 'nlu.md')
        console.log('saving bm:', x)
        fs.writeFileSync(x, req.body.bm)
        console.log('BM:', req.body.bm.substr(0, 200))
    }
    if (req.body.kbs) {
        const x = path.resolve(__dirname, '../../chatbot/worker_kbs', 'nlu.md')
        console.log('saving kbs:', x)
        fs.writeFileSync(x, req.body.kbs)
    }
    if (req.body.csv) {
        const x = path.resolve(__dirname, '../../chatbot/worker_kbs', 'bot_responses.csv')
        console.log('saving csv:', x)
        fs.writeFileSync(x, req.body.csv)
    }
    res.json(true)
})
router.route('/files').post((req, res) => {
    if (req.body.bm) {
        console.log('saving bm')
        const x = path.resolve(__dirname, '../../chatbot/worker_bm', 'nlu.md')
        fs.writeFileSync(x, req.body.bm)
    }
    if (req.body.kbs) {
        console.log('saving kbs')
        const x = path.resolve(__dirname, '../../chatbot/worker_kbs', 'nlu.md')
        fs.writeFileSync(x, req.body.kbs)
    }
    if (req.body.csv) {
        console.log('saving csv')
        const x = path.resolve(__dirname, '../../chatbot/worker_kbs', 'bot_responses.csv')
        fs.writeFileSync(x, req.body.csv)
    }
    res.json(true)
})

module.exports = router
