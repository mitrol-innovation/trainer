/* eslint-disable no-undef */
import { store } from 'quasar/wrappers'
import { createStore } from 'vuex'
import bot from '../pages/bot/store'


export default store(function (/* { ssrContext } */) {
    const Store = createStore({
        modules: {
            bot
        },
        strict: process.env.DEBUGGING
    })

    return Store
})
