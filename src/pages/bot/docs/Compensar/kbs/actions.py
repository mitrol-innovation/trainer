from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet
from typing import Any, Text, Dict, List, Union, Optional
import csv
import numpy as np
import json
from datetime import datetime, timedelta
from pytz import timezone
import calendar
from datetime import date




class ActionPasameAgente(Action):
    def name(self):
        return "action_ActionPasameAgente"

    def run(self, dispatcher, tracker, domain):
        intent_index=[]
        with open(r'/home/emiliano/Desktop/BOTS/compensar/alojamiento/alojamiento_kbs/actions/respuestas_bot_alojamiento.csv', encoding='utf-8') as csvfile:
            reader=csv.reader(csvfile)
            for row in reader:
                intent_index.append(row)
        intent_index=np.array(intent_index)

        k=np.where(intent_index[:,0]=="Transfiere_En_Horario")[0][0]
        j=np.where(intent_index[:,0]=="Transfiere_Fuera_De_Horario")[0][0]
        message_Transfiere_en_Horario=intent_index[k,1]
        message_Transfiere_Fuera_De_Horario=intent_index[j,1]
        now_colombia = datetime.now(timezone('America/Bogota'))
        UltimaHora= now_colombia.strftime("%Y-%m-%d %H:%M:%S %Z%z")
        Colombia7am = now_colombia.replace(hour=7, minute=00, second=0, microsecond=0)
        Colombia8pm = now_colombia.replace(hour=20, minute=00, second=0, microsecond=0)
        Colombia5pm = now_colombia.replace(hour=17, minute=00, second=0, microsecond=0)
        Colombia6pm = now_colombia.replace(hour=18, minute=00, second=0, microsecond=0)
        Colombia10am = now_colombia.replace(hour=10, minute=00, second=0, microsecond=0)
        Colombia8am = now_colombia.replace(hour=8, minute=00, second=0, microsecond=0)
        daysLaV = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]
        daysS = ["Saturday"]
        daysD = ["Sunday"]
        dia = calendar.day_name[date.today().weekday()]
        dia_calendario=date.today()
        feriados_colombia=[date(2020,3,23),date(2020,4,5),date(2020,4,9),date(2020,4,10),date(2020,4,12),
                            date(2020,5,1),date(2020,5,25),date(2020,6,15),date(2020,6,22),date(2020,6,29),
                            date(2020,7,20),date(2020,8,7),date(2020,8,17),date(2020,10,12),date(2020,11,2),
                            date(2020,11,16),date(2020,12,8),date(2020,12,25),date(2021,1,1),date(2021,1,11),
                            date(2021,3,22),date(2021,3,28),date(2021,4,1),date(2021,4,2),date(2021,4,4),
                            date(2021,5,1),date(2021,5,17),date(2021,6,7),date(2021,6,14),date(2021,7,5),
                            date(2021,7,20),date(2021,8,7),date(2021,8,16),date(2021,10,18),date(2021,11,1),
                            date(2021,11,15),date(2021,12,8),date(2021,12,25)]

        if (dia in daysD) or (dia_calendario in feriados_colombia):
                if Colombia10am < now_colombia < Colombia6pm:
                    dispatcher.utter_message(message_Transfiere_en_Horario)
                else:
                    dispatcher.utter_message(message_Transfiere_Fuera_De_Horario)
        if dia in daysLaV:
                if Colombia7am < now_colombia < Colombia8pm:
                    dispatcher.utter_message(message_Transfiere_en_Horario)
                else:
                    dispatcher.utter_message(message_Transfiere_Fuera_De_Horario)
        if dia in daysS:
                if Colombia8am < now_colombia < Colombia5pm:
                    dispatcher.utter_message(message_Transfiere_en_Horario)
                else:
                    dispatcher.utter_message(message_Transfiere_Fuera_De_Horario)

        return [SlotSet('UltimaHora',UltimaHora)]


class ReservasMediosHacerReserva(Action):
    def name(self):
        return "action_Reservas_MediosHacerReserva"

    def run(self, dispatcher, tracker, domain):
        intent_index=[]
        with open(r'/home/emiliano/Desktop/BOTS/compensar/alojamiento/alojamiento_kbs/actions/respuestas_bot_alojamiento.csv', encoding='utf-8') as csvfile:
            reader=csv.reader(csvfile)
            for row in reader:
                intent_index.append(row)
        intent_index=np.array(intent_index)

        k=np.where(intent_index[:,0]=="Reservas_MediosHacerReserva_Transferencia")[0][0]
        j=np.where(intent_index[:,0]=="Reservas_MediosHacerReserva")[0][0]
        message_Reservas_MediosHacerReserva_Transferencia=intent_index[k,1]
        message_Reservas_MediosHacerReserva=intent_index[j,1]
        now_colombia = datetime.now(timezone('America/Bogota'))
        UltimaHora= now_colombia.strftime("%Y-%m-%d %H:%M:%S %Z%z")
        Colombia7am = now_colombia.replace(hour=7, minute=00, second=0, microsecond=0)
        Colombia8pm = now_colombia.replace(hour=20, minute=00, second=0, microsecond=0)
        Colombia5pm = now_colombia.replace(hour=17, minute=00, second=0, microsecond=0)
        Colombia6pm = now_colombia.replace(hour=18, minute=00, second=0, microsecond=0)
        Colombia10am = now_colombia.replace(hour=10, minute=00, second=0, microsecond=0)
        Colombia8am = now_colombia.replace(hour=8, minute=00, second=0, microsecond=0)
        daysLaV = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]
        daysS = ["Saturday"]
        daysD = ["Sunday"]
        dia = calendar.day_name[date.today().weekday()]
        dia_calendario=date.today()
        feriados_colombia=[date(2020,3,23),date(2020,4,5),date(2020,4,9),date(2020,4,10),date(2020,4,12),
                            date(2020,5,1),date(2020,5,25),date(2020,6,15),date(2020,6,22),date(2020,6,29),
                            date(2020,7,20),date(2020,8,7),date(2020,8,17),date(2020,10,12),date(2020,11,2),
                            date(2020,11,16),date(2020,12,8),date(2020,12,25),date(2021,1,1),date(2021,1,11),
                            date(2021,3,22),date(2021,3,28),date(2021,4,1),date(2021,4,2),date(2021,4,4),
                            date(2021,5,1),date(2021,5,17),date(2021,6,7),date(2021,6,14),date(2021,7,5),
                            date(2021,7,20),date(2021,8,7),date(2021,8,16),date(2021,10,18),date(2021,11,1),
                            date(2021,11,15),date(2021,12,8),date(2021,12,25)]

        if (dia in daysD) or (dia_calendario in feriados_colombia):
                if Colombia10am < now_colombia < Colombia6pm:
                    dispatcher.utter_message(message_Reservas_MediosHacerReserva_Transferencia)
                else:
                    dispatcher.utter_message(message_Reservas_MediosHacerReserva)               
        if dia in daysLaV:
                if Colombia7am < now_colombia < Colombia8pm:
                    dispatcher.utter_message(message_Reservas_MediosHacerReserva_Transferencia)
                else:
                    dispatcher.utter_message(message_Reservas_MediosHacerReserva)
        if dia in daysS:
                if Colombia8am < now_colombia < Colombia5pm:
                    dispatcher.utter_message(message_Reservas_MediosHacerReserva_Transferencia)
                else:
                    dispatcher.utter_message(message_Reservas_MediosHacerReserva)

        return [SlotSet('UltimaHora',UltimaHora)]


class PlanesLagosol(Action):
    def name(self):
        return "action_PlanesLagosol"

    def run(self, dispatcher, tracker, domain):
        intent_index=[]
        with open(r'/home/emiliano/Desktop/BOTS/compensar/alojamiento/alojamiento_kbs/actions/respuestas_bot_alojamiento.csv', encoding='utf-8') as csvfile:
            reader=csv.reader(csvfile)
            for row in reader:
                intent_index.append(row)
        intent_index=np.array(intent_index)

        k=np.where(intent_index[:,0]=="Lagosol_Planes_Linea")[0][0]
        j=np.where(intent_index[:,0]=="Lagosol_Planes_Fuera_Linea")[0][0]
        message_Lagosol_Planes_Linea=intent_index[k,1]
        message_Lagosol_Planes_Fuera_Linea=intent_index[j,1]
        now_colombia = datetime.now(timezone('America/Bogota'))
        UltimaHora= now_colombia.strftime("%Y-%m-%d %H:%M:%S %Z%z")
        Colombia7am = now_colombia.replace(hour=7, minute=00, second=0, microsecond=0)
        Colombia8pm = now_colombia.replace(hour=20, minute=00, second=0, microsecond=0)
        Colombia5pm = now_colombia.replace(hour=17, minute=00, second=0, microsecond=0)
        Colombia6pm = now_colombia.replace(hour=18, minute=00, second=0, microsecond=0)
        Colombia10am = now_colombia.replace(hour=10, minute=00, second=0, microsecond=0)
        Colombia8am = now_colombia.replace(hour=8, minute=00, second=0, microsecond=0)
        daysLaV = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]
        daysS = ["Saturday"]
        daysD = ["Sunday"]
        dia = calendar.day_name[date.today().weekday()]
        dia_calendario=date.today()
        feriados_colombia=[date(2020,3,23),date(2020,4,5),date(2020,4,9),date(2020,4,10),date(2020,4,12),
                            date(2020,5,1),date(2020,5,25),date(2020,6,15),date(2020,6,22),date(2020,6,29),
                            date(2020,7,20),date(2020,8,7),date(2020,8,17),date(2020,10,12),date(2020,11,2),
                            date(2020,11,16),date(2020,12,8),date(2020,12,25),date(2021,1,1),date(2021,1,11),
                            date(2021,3,22),date(2021,3,28),date(2021,4,1),date(2021,4,2),date(2021,4,4),
                            date(2021,5,1),date(2021,5,17),date(2021,6,7),date(2021,6,14),date(2021,7,5),
                            date(2021,7,20),date(2021,8,7),date(2021,8,16),date(2021,10,18),date(2021,11,1),
                            date(2021,11,15),date(2021,12,8),date(2021,12,25)]

        if (dia in daysD) or (dia_calendario in feriados_colombia):
                if Colombia10am < now_colombia < Colombia6pm:
                    dispatcher.utter_message(message_Lagosol_Planes_Linea)
                else:
                    dispatcher.utter_message(message_Lagosol_Planes_Fuera_Linea)
        if dia in daysLaV:
                if Colombia7am < now_colombia < Colombia8pm:
                    dispatcher.utter_message(message_Lagosol_Planes_Linea)
                else:
                    dispatcher.utter_message(message_Lagosol_Planes_Fuera_Linea)
        if dia in daysS:
                if Colombia8am < now_colombia < Colombia5pm:
                    dispatcher.utter_message(message_Lagosol_Planes_Linea)
                else:
                    dispatcher.utter_message(message_Lagosol_Planes_Fuera_Linea)

        return [SlotSet('UltimaHora',UltimaHora)]



class PlanesLagomar(Action):
    def name(self):
        return "action_PlanesLagomar"

    def run(self, dispatcher, tracker, domain):
        intent_index=[]
        with open(r'/home/emiliano/Desktop/BOTS/compensar/alojamiento/alojamiento_kbs/actions/respuestas_bot_alojamiento.csv', encoding='utf-8') as csvfile:
            reader=csv.reader(csvfile)
            for row in reader:
                intent_index.append(row)
        intent_index=np.array(intent_index)

        k=np.where(intent_index[:,0]=="Lagomar_Planes_Linea")[0][0]
        j=np.where(intent_index[:,0]=="Lagomar_Planes_Fuera_Linea")[0][0]
        message_Lagomar_Planes_Linea=intent_index[k,1]
        message_Lagomar_Planes_Fuera_Linea=intent_index[j,1]
        now_colombia = datetime.now(timezone('America/Bogota'))
        UltimaHora= now_colombia.strftime("%Y-%m-%d %H:%M:%S %Z%z")
        Colombia7am = now_colombia.replace(hour=7, minute=00, second=0, microsecond=0)
        Colombia8pm = now_colombia.replace(hour=20, minute=00, second=0, microsecond=0)
        Colombia5pm = now_colombia.replace(hour=17, minute=00, second=0, microsecond=0)
        Colombia6pm = now_colombia.replace(hour=18, minute=00, second=0, microsecond=0)
        Colombia10am = now_colombia.replace(hour=10, minute=00, second=0, microsecond=0)
        Colombia8am = now_colombia.replace(hour=8, minute=00, second=0, microsecond=0)
        daysLaV = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]
        daysS = ["Saturday"]
        daysD = ["Sunday"]
        dia = calendar.day_name[date.today().weekday()]
        dia_calendario=date.today()

        feriados_colombia=[date(2020,3,23),date(2020,4,5),date(2020,4,9),date(2020,4,10),date(2020,4,12),
                            date(2020,5,1),date(2020,5,25),date(2020,6,15),date(2020,6,22),date(2020,6,29),
                            date(2020,7,20),date(2020,8,7),date(2020,8,17),date(2020,10,12),date(2020,11,2),
                            date(2020,11,16),date(2020,12,8),date(2020,12,25),date(2021,1,1),date(2021,1,11),
                            date(2021,3,22),date(2021,3,28),date(2021,4,1),date(2021,4,2),date(2021,4,4),
                            date(2021,5,1),date(2021,5,17),date(2021,6,7),date(2021,6,14),date(2021,7,5),
                            date(2021,7,20),date(2021,8,7),date(2021,8,16),date(2021,10,18),date(2021,11,1),
                            date(2021,11,15),date(2021,12,8),date(2021,12,25)]

        if (dia in daysD) or (dia_calendario in feriados_colombia):
                if Colombia10am < now_colombia < Colombia6pm:
                    print('Negriii')
                    dispatcher.utter_message(message_Lagomar_Planes_Linea)
                else:
                    dispatcher.utter_message(message_Lagomar_Planes_Fuera_Linea)
        if dia in daysLaV:
                if Colombia7am < now_colombia < Colombia8pm:
                    dispatcher.utter_message(message_Lagomar_Planes_Linea)
                else:
                    dispatcher.utter_message(message_Lagomar_Planes_Fuera_Linea)
        if dia in daysS:
                if Colombia8am < now_colombia < Colombia5pm:
                    dispatcher.utter_message(message_Lagomar_Planes_Linea)
                else:
                    dispatcher.utter_message(message_Lagomar_Planes_Fuera_Linea)

        return [SlotSet('UltimaHora',UltimaHora)]



class ReservasCambiosReserva(Action):
    def name(self):
        return "action_Reservas_CambiosReserva"

    def run(self, dispatcher, tracker, domain):
        intent_index=[]
        with open(r'/home/emiliano/Desktop/BOTS/compensar/alojamiento/alojamiento_kbs/actions/respuestas_bot_alojamiento.csv', encoding='utf-8') as csvfile:
            reader=csv.reader(csvfile)
            for row in reader:
                intent_index.append(row)
        intent_index=np.array(intent_index)

        k=np.where(intent_index[:,0]=="Reservas_CambiosReserva_Transferencia")[0][0]
        j=np.where(intent_index[:,0]=="Reservas_CambiosReserva")[0][0]
        message_Reservas_CambiosReserva_Transferencia=intent_index[k,1]
        message_Reservas_CambiosReserva=intent_index[j,1]
        now_colombia = datetime.now(timezone('America/Bogota'))
        UltimaHora= now_colombia.strftime("%Y-%m-%d %H:%M:%S %Z%z")
        Colombia7am = now_colombia.replace(hour=7, minute=00, second=0, microsecond=0)
        Colombia8pm = now_colombia.replace(hour=20, minute=00, second=0, microsecond=0)
        Colombia5pm = now_colombia.replace(hour=17, minute=00, second=0, microsecond=0)
        Colombia6pm = now_colombia.replace(hour=18, minute=00, second=0, microsecond=0)
        Colombia10am = now_colombia.replace(hour=10, minute=00, second=0, microsecond=0)
        Colombia8am = now_colombia.replace(hour=8, minute=00, second=0, microsecond=0)
        daysLaV = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]
        daysS = ["Saturday"]
        daysD = ["Sunday"]
        dia = calendar.day_name[date.today().weekday()]
        dia_calendario=date.today()
        feriados_colombia=[date(2020,3,23),date(2020,4,5),date(2020,4,9),date(2020,4,10),date(2020,4,12),
                            date(2020,5,1),date(2020,5,25),date(2020,6,15),date(2020,6,22),date(2020,6,29),
                            date(2020,7,20),date(2020,8,7),date(2020,8,17),date(2020,10,12),date(2020,11,2),
                            date(2020,11,16),date(2020,12,8),date(2020,12,25),date(2021,1,1),date(2021,1,11),
                            date(2021,3,22),date(2021,3,28),date(2021,4,1),date(2021,4,2),date(2021,4,4),
                            date(2021,5,1),date(2021,5,17),date(2021,6,7),date(2021,6,14),date(2021,7,5),
                            date(2021,7,20),date(2021,8,7),date(2021,8,16),date(2021,10,18),date(2021,11,1),
                            date(2021,11,15),date(2021,12,8),date(2021,12,25)]

        if (dia in daysD) or (dia_calendario in feriados_colombia):
                if Colombia10am < now_colombia < Colombia6pm:
                    dispatcher.utter_message(message_Reservas_CambiosReserva_Transferencia)
                else:
                    dispatcher.utter_message(message_Reservas_CambiosReserva)
        if dia in daysLaV:
                if Colombia7am < now_colombia < Colombia8pm:
                    dispatcher.utter_message(message_Reservas_CambiosReserva_Transferencia)
                else:
                    dispatcher.utter_message(message_Reservas_CambiosReserva)
        if dia in daysS:
                if Colombia8am < now_colombia < Colombia5pm:
                    dispatcher.utter_message(message_Reservas_CambiosReserva_Transferencia)
                else:
                    dispatcher.utter_message(message_Reservas_CambiosReserva)

        return [SlotSet('UltimaHora',UltimaHora)]





class ActionTarifasPasadiaPorCategoria(Action):
    def name(self):
        return "action_TarifasPasadiaPorCategoria"

    def run(self, dispatcher, tracker, domain):
        print("action_tarifas")
        categoria=tracker.get_slot("categorias")
        intent_index=[]
        with open(r'/home/emiliano/Desktop/BOTS/compensar/alojamiento/alojamiento_kbs/actions/respuestas_bot_alojamiento.csv', encoding='utf-8') as csvfile:
            reader=csv.reader(csvfile)
            for row in reader:
                intent_index.append(row)
        intent_index=np.array(intent_index)

        k=np.where(intent_index[:,0]=="Costo_pasadia")[0][0]
        a=np.where(intent_index[:,0]=="Costo_pasadia_Categoria_A")[0][0]
        b=np.where(intent_index[:,0]=="Costo_pasadia_Categoria_B")[0][0]
        c=np.where(intent_index[:,0]=="Costo_pasadia_Categoria_C")[0][0]

        message_Costo_Pasadia=intent_index[k,1]
        message_Costo_pasadia_Categoria_A=intent_index[a,1]
        message_Costo_pasadia_Categoria_B=intent_index[b,1]
        message_Costo_pasadia_Categoria_C=intent_index[c,1]
        if categoria==None:
            dispatcher.utter_message(message_Costo_Pasadia)     

        if categoria=="A" or categoria=="a":
            dispatcher.utter_message(message_Costo_pasadia_Categoria_A)
            
        if categoria=="B" or categoria=="b":
            dispatcher.utter_message(message_Costo_pasadia_Categoria_B)
            
        if categoria=="C" or categoria=="c":
            dispatcher.utter_message(message_Costo_pasadia_Categoria_C)
            

        return[] 


class ActionInfoKb(Action):
    def name(self) -> Text:
        return "info_kb"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        intent=tracker.latest_message['intent']['name']
        intent_index=[]
        
        with open(r'/home/emiliano/Desktop/BOTS/compensar/alojamiento/alojamiento_kbs/actions/respuestas_bot_alojamiento.csv', encoding='utf-8') as csvfile:
            reader=csv.reader(csvfile)
            for row in reader:
                intent_index.append(row)
        intent_index=np.array(intent_index)
        i=np.where(intent_index[:,0]==intent)[0][0]

        message=intent_index[i,1]

        dispatcher.utter_message(message)

        return []

class ActionDefault(Action):

    def name(self) -> Text:
        return "action_DefaultMitrol"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        #print(json.loads(vars(tracker)))
        
        trackerr=json.dumps(vars(tracker))
        trackerDict=json.loads(trackerr)
        confidence=[]
        for i in range(0,len(trackerDict['events'])):
            if trackerDict['events'][i]['event']=='user':
                confidence.append(trackerDict['events'][i]['parse_data']['intent']['confidence'])
        t=list(np.where(np.array(confidence)<0.75)[0])
        for i in range(0,len(t)):
            if i%2==0:
                confidence[t[i]]=-1
            else:
                confidence[t[i]]=-2

        intent_index=[]
        
        with open(r'/home/emiliano/Desktop/BOTS/compensar/alojamiento/alojamiento_kbs/actions/respuestas_bot_alojamiento.csv', encoding='utf-8') as csvfile:
            reader=csv.reader(csvfile)
            for row in reader:
                intent_index.append(row)
        intent_index=np.array(intent_index)

        t=np.where(intent_index[:,0]=="Default_Transferencia_En_Horario")[0][0]
        tf=np.where(intent_index[:,0]=="Default_Transferencia_Fuera_De_Horario")[0][0]
        d1=np.where(intent_index[:,0]=="Default_1")[0][0]
        d2=np.where(intent_index[:,0]=="Default_2")[0][0]

        message_Default_Transferencia_En_Horario=intent_index[t,1]
        message_Default_Transferencia_Fuera_De_Horario=intent_index[tf,1]
        message_Default_1=intent_index[d1,1]
        message_Default_2=intent_index[d2,1]

        now_colombia = datetime.now(timezone('America/Bogota'))
        UltimaHora= now_colombia.strftime("%Y-%m-%d %H:%M:%S %Z%z")
        Colombia7am = now_colombia.replace(hour=7, minute=00, second=0, microsecond=0)
        Colombia8pm = now_colombia.replace(hour=20, minute=00, second=0, microsecond=0)
        Colombia5pm = now_colombia.replace(hour=17, minute=00, second=0, microsecond=0)
        Colombia6pm = now_colombia.replace(hour=18, minute=00, second=0, microsecond=0)
        Colombia10am = now_colombia.replace(hour=10, minute=00, second=0, microsecond=0)
        Colombia8am = now_colombia.replace(hour=8, minute=00, second=0, microsecond=0)
        daysLaV = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]
        daysS = ["Saturday"]
        daysD = ["Sunday"]
        dia = calendar.day_name[date.today().weekday()]
        dia_calendario=date.today()
        feriados_colombia=[date(2020,3,23),date(2020,4,5),date(2020,4,9),date(2020,4,10),date(2020,4,12),
                            date(2020,5,1),date(2020,5,25),date(2020,6,15),date(2020,6,22),date(2020,6,29),
                            date(2020,7,20),date(2020,8,7),date(2020,8,17),date(2020,10,12),date(2020,11,2),
                            date(2020,11,16),date(2020,12,8),date(2020,12,25),date(2021,1,1),date(2021,1,11),
                            date(2021,3,22),date(2021,3,28),date(2021,4,1),date(2021,4,2),date(2021,4,4),
                            date(2021,5,1),date(2021,5,17),date(2021,6,7),date(2021,6,14),date(2021,7,5),
                            date(2021,7,20),date(2021,8,7),date(2021,8,16),date(2021,10,18),date(2021,11,1),
                            date(2021,11,15),date(2021,12,8),date(2021,12,25)]

        if (dia in daysD) or (dia_calendario in feriados_colombia):
                if Colombia10am < now_colombia < Colombia6pm:
                        for i in range(len(confidence)-1,len(confidence)-3,-1):
                                if (i>0 and confidence[i-2]==-1 and confidence[i-1]==-2 and confidence[i]==-1) or (i>0 and confidence[i-2]==-2 and confidence[i-1]==-1 and confidence[i]==-2):
                                        dispatcher.utter_message(message_Default_Transferencia_En_Horario)
                                        return [SlotSet('UltimaHora',UltimaHora)]
                                if i>0 and confidence[i]==-1 or confidence[0]==-1:
                                        dispatcher.utter_message(message_Default_1)
                                        break
                                if i>0 and confidence[i]==-2:
                                        dispatcher.utter_message(message_Default_2)
                                        break

                        return []
                else:
                        for i in range(len(confidence)-1,len(confidence)-3,-1):
                                if (i>0 and confidence[i-2]==-1 and confidence[i-1]==-2 and confidence[i]==-1) or (i>0 and confidence[i-2]==-2 and confidence[i-1]==-1 and confidence[i]==-2):
                                        dispatcher.utter_message(message_Default_Transferencia_Fuera_De_Horario)
                                        return [SlotSet('UltimaHora',UltimaHora)]
                                if i>0 and confidence[i]==-1 or confidence[0]==-1:
                                        dispatcher.utter_message(message_Default_1)
                                        break
                                if i>0 and confidence[i]==-2:
                                        dispatcher.utter_message(message_Default_2)
                                        break
                        return[]
        if dia in daysLaV:
                if Colombia7am < now_colombia < Colombia8pm:
                        for i in range(len(confidence)-1,len(confidence)-3,-1):
                                if (i>0 and confidence[i-2]==-1 and confidence[i-1]==-2 and confidence[i]==-1) or (i>0 and confidence[i-2]==-2 and confidence[i-1]==-1 and confidence[i]==-2):
                                        dispatcher.utter_message(message_Default_Transferencia_En_Horario)
                                        return [SlotSet('UltimaHora',UltimaHora)]
                                if i>0 and confidence[i]==-1 or confidence[0]==-1:
                                        dispatcher.utter_message(message_Default_1)
                                        break
                                if i>0 and confidence[i]==-2:
                                        dispatcher.utter_message(message_Default_2)
                                        break

                        return []
                else:
                        for i in range(len(confidence)-1,len(confidence)-3,-1):
                                if (i>0 and confidence[i-2]==-1 and confidence[i-1]==-2 and confidence[i]==-1) or (i>0 and confidence[i-2]==-2 and confidence[i-1]==-1 and confidence[i]==-2):
                                        dispatcher.utter_message(message_Default_Transferencia_Fuera_De_Horario)
                                        return [SlotSet('UltimaHora',UltimaHora)]
                                if i>0 and confidence[i]==-1 or confidence[0]==-1:
                                        dispatcher.utter_message(message_Default_1)
                                        break
                                if i>0 and confidence[i]==-2:
                                        dispatcher.utter_message(message_Default_2)
                                        break
                        return[]

        if dia in daysS:
                if Colombia8am < now_colombia < Colombia5pm:
                        for i in range(len(confidence)-1,len(confidence)-3,-1):
                                if (i>0 and confidence[i-2]==-1 and confidence[i-1]==-2 and confidence[i]==-1) or (i>0 and confidence[i-2]==-2 and confidence[i-1]==-1 and confidence[i]==-2):
                                        dispatcher.utter_message(message_Default_Transferencia_En_Horario)
                                        return [SlotSet('UltimaHora',UltimaHora)]
                                if i>0 and confidence[i]==-1 or confidence[0]==-1:
                                        dispatcher.utter_message(message_Default_1)
                                        break
                                if i>0 and confidence[i]==-2:
                                        dispatcher.utter_message(message_Default_2)
                                        break

                        return []
                else:
                        for i in range(len(confidence)-1,len(confidence)-3,-1):
                                if (i>0 and confidence[i-2]==-1 and confidence[i-1]==-2 and confidence[i]==-1) or (i>0 and confidence[i-2]==-2 and confidence[i-1]==-1 and confidence[i]==-2):
                                        dispatcher.utter_message(message_Default_Transferencia_Fuera_De_Horario)
                                        return [SlotSet('UltimaHora',UltimaHora)]
                                if i>0 and confidence[i]==-1 or confidence[0]==-1:
                                        dispatcher.utter_message(message_Default_1)
                                        break
                                if i>0 and confidence[i]==-2:
                                        dispatcher.utter_message(message_Default_2)
                                        break
                        return[]



neededParams = ['InteractionId', 'UserName', 'UserMail', 'MimeType', 'Parameters', 'user.mail', 'user.name']
from datetime import datetime
class bcolors:
    HEADER = '\033[95m'
    INFO = '\033[0;37;44m INFO \033[0m'
    WARNING = '\033[0;37;43m WARNING \033[0m'
    ERROR = '\033[0;37;41m ERROR \033[0m'
    ORIGIN = '\033[3;34;40m'
    OBJECT = '\033[1;35;40m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
