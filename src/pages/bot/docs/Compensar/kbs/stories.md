

## Handoff to human
* Transferencia_Solicitud
    - action_ActionPasameAgente

## General_Saludar
* General_Saludo
    - utter_General_Saludo

## General_Despedida    
* General_Despedida
    - utter_General_Despedida


## niega
* niega
    - utter_General_Despedida


## General_Despedida+Agradece
*General_Despedida+Agradece
    - utter_General_Despedida



## General_ActualizarDatos
* General_ActualizarDatos
    - info_kb


## Afiliaciones_ConocerCategoria
* Afiliaciones_ConocerCategoria
    - info_kb

## Afiliaciones_AfiliacionCosto
* Afiliaciones_AfiliacionCosto
    - info_kb



## Afiliaciones_Invitados
* Afiliaciones_Invitados
    - info_kb

## Afiliaciones_AsignacionCategoria
* Afiliaciones_AsignacionCategoria
    - info_kb

## Afiliaciones_AQuienAfiliar
* Afiliaciones_AQuienAfiliar
    - info_kb

## AlimentosBebidas_PlanesIncluyenBebidas
* AlimentosBebidas_PlanesIncluyenBebidas
    - info_kb

## Afiliaciones_MediosConsultarGrupoFamiliar
* Afiliaciones_MediosConsultarGrupoFamiliar
    - info_kb


## AlimentosBebidas_MenuInfantil
* AlimentosBebidas_MenuInfantil
    - info_kb

## AlimentosBebidas_HorarioComidasLagosol
* AlimentosBebidas_HorarioComidasLagosol
    - info_kb

## AlimentosBebidas_HorarioComidasLagomar
* AlimentosBebidas_HorarioComidasLagomar
    - info_kb

## AlimentosBebidas_TipoAlimentacion
* AlimentosBebidas_TipoAlimentacion
    - info_kb

## AlimentosBebidas_VendenBebidasAlcoholicas
* AlimentosBebidas_VendenBebidasAlcoholicas
    - info_kb

## AlimentosBebidas_PlatosCarta
* AlimentosBebidas_PlatosCarta
    - info_kb

## Canal_HorarioChat
* Canal_HorarioChat
    - info_kb

## Convenios_ReservarHotelesEstelar
* Convenios_ReservarHotelesEstelar
    - info_kb

## Canal_horarioLineaTelefonica
* Canal_horarioLineaTelefonica
    - info_kb

## Canal_OficinasHorarios
* Canal_OficinasHorarios
    - info_kb


## Pagos_TiempoPagoReserva
* Pagos_TiempoPagoReserva
    - info_kb

## Pagos_PagarConLibranzaNomina
* Pagos_PagarConLibranzaNomina
    - info_kb

## Planes_PlanesNavidadFinDeAnio
* Planes_PlanesNavidadFinDeAnio
    - info_kb


## Reservas_Requisitos_RealizarReserva
* Reservas_Requisitos_RealizarReserva
    - info_kb

## Reservas_CambiosReserva
* Reservas_CambiosReserva
    - action_Reservas_CambiosReserva


## Reservas_MediosCancelacionReservas
* Reservas_MediosCancelacionReservas
    - info_kb


## Reservas_CambiosConservandoTarifa
* Reservas_CambiosConservandoTarifa
    - info_kb

## Reservas_CancelarReactivarReserva
* Reservas_CancelarReactivarReserva
    - info_kb

## Reservas_MediosHacerReserva
* Reservas_MediosHacerReserva
    - action_Reservas_MediosHacerReserva

## Lagomar_ascensores
* Lagomar_ascensores
    - info_kb

## Lagomar_No_Pasadia
* Lagomar_No_Pasadia
    - info_kb

## Pasadia_Adquirir_Boletas
* Pasadia_Adquirir_Boletas
    - info_kb

## Reservas_MotivoCancelacionReserva
* Reservas_MotivoCancelacionReserva
    - info_kb


## Reservas_ProblemasPortalReservas
* Reservas_ProblemasPortalReservas
    - info_kb

## Reservas_PlanearEsAhorrar
* Reservas_PlanearEsAhorrar
    - info_kb

## Reservas_EdadPagoNiños
* Reservas_EdadPagoNiños
    - info_kb

## Reservas_CostoHospedaje
* Reservas_CostoHospedaje
    - info_kb

## Reservas_DisponibilidadTarifas
* Reservas_DisponibilidadTarifas
    - info_kb

## Reservas_CederReserva
* Reservas_CederReserva
    - info_kb


## Reservas_LimiteAcompañantes
* Reservas_LimiteAcompañantes
    - info_kb


## Pagos_MediosPagosReserva
* Pagos_MediosPagosReserva
    - info_kb

## CoronaVirus
* CoronaVirus
    - info_kb


## CoronaVirus_2
* CoronaVirus_2
    - info_kb
* Afirma
    - action_ActionPasameAgente


## CoronaVirus_2
* CoronaVirus_2
    - info_kb
* niega
    - utter_OfreceMasAyuda

## CoronaVirus_3
* CoronaVirus_3
    - info_kb


## CoronaVirus_4
* CoronaVirus_4
    - info_kb

## SobreHoteles_UbicacionHoteles
* SobreHoteles_UbicacionHoteles
    - info_kb

## SobreHoteles_DocumentosParaCheckIn
* SobreHoteles_DocumentosParaCheckIn
    - info_kb

## SobreHoteles_HorarioIngresoHoteles
* SobreHoteles_HorarioIngresoHoteles
    - info_kb


## SobreHoteles_TransporteHoteles
* SobreHoteles_TransporteHoteles
    - info_kb


## SobreHoteles_PermiteIngresoAlimentos
* SobreHoteles_PermiteIngresoAlimentos
    - info_kb



## SobreHoteles_IngresoMascotas
* SobreHoteles_IngresoMascotas
    - info_kb

## SobreHoteles_PiscinaNiños
* SobreHoteles_PiscinaNiños
    - info_kb

## SobreHoteles_HabitacionesAireAcondicionado
* SobreHoteles_HabitacionesAireAcondicionado
    - info_kb

## SobreHoteles_ServicioParqueadero
* SobreHoteles_ServicioParqueadero
    - info_kb

## SobreHoteles_SillaDeRuedas
* SobreHoteles_SillaDeRuedas
    - info_kb

## SobreHoteles_PlanesRomanticos
* SobreHoteles_PlanesRomanticos
    - info_kb



## SobreHoteles_Lavanderia
* SobreHoteles_Lavanderia
    - info_kb


## SobreHoteles_HotelTiendas
 * SobreHoteles_HotelTiendas
    - info_kb


## SobreHoteles_CanchaTenis
* SobreHoteles_CanchaTenis
    - info_kb


## SobreHoteles_DeportesExtremos
* SobreHoteles_DeportesExtremos
    - info_kb

## SobreHoteles_ServiciosAdicionales
* SobreHoteles_ServiciosAdicionales
    - info_kb

## SobreHoteles_Enfermeria
* SobreHoteles_Enfermeria
    - info_kb

## SobreHoteles_PiscinasHotel
* SobreHoteles_PiscinasHotel
    - info_kb


## SobreHoteles_TVHabitacion
* SobreHoteles_TVHabitacion
    - info_kb


## SobreHoteles_HorarioRecepcion
* SobreHoteles_HorarioRecepcion
    - info_kb



## SobreHoteles_SalirHotel
* SobreHoteles_SalirHotel
    - info_kb
## SobreHoteles_MaterialCanchaTenis
* SobreHoteles_MaterialCanchaTenis
    - info_kb

## SobreHoteles_NadarLago
* SobreHoteles_NadarLago
    - info_kb

## SobreHoteles_AlojamientoConAlimentacion
* SobreHoteles_AlojamientoConAlimentacion
    - info_kb



## SobreHoteles_TiposHabitacionLagosol
* SobreHoteles_TiposHabitacionLagosol
    - info_kb

## SobreHoteles_CumpleañosLagosol
* SobreHoteles_CumpleañosLagosol
    - info_kb

## SobreHoteles_TiposHabitacionLagomar
* SobreHoteles_TiposHabitacionLagomar
    - info_kb

## AlimentosBebidas_ComidasRapidasLagomar
* AlimentosBebidas_ComidasRapidasLagomar
    - info_kb

## AlimentosBebidas_SnaksBebidasLagomar
* AlimentosBebidas_SnaksBebidasLagomar
    - info_kb


## SobreHoteles_CumpleañosLagomar
* SobreHoteles_CumpleañosLagomar
    - info_kb

## SobreHoteles_WIFIHoteles
* SobreHoteles_WIFIHoteles
    - info_kb

## SobreHoteles_GolfLagomar
* SobreHoteles_GolfLagomar
    - info_kb

## SobreHoteles_VoleyPlayaLagomar
* SobreHoteles_VoleyPlayaLagomar
    - info_kb


## SobreHoteles_SalonJuegosLagomar
* SobreHoteles_SalonJuegosLagomar
    - info_kb

## SobreHoteles_CanopyLagomar
* SobreHoteles_CanopyLagomar
    - info_kb

## SobreHoteles_SPALagomar
* SobreHoteles_SPALagomar
    - info_kb

## SobreHoteles_FutbolLagomar
* SobreHoteles_FutbolLagomar
    - info_kb

## SobreHoteles_PlayaLagomar
* SobreHoteles_PlayaLagomar   
    - info_kb

## SobreHoteles_BolosLagomar
* SobreHoteles_BolosLagomar
    - info_kb

## SobreHoteles_SalonJuegosLagosol
* SobreHoteles_SalonJuegosLagosol
    - info_kb


## SobreHoteles_FutbolLagosol
* SobreHoteles_FutbolLagosol
    - info_kb

## SobreHoteles_BicicletasAcuaticasLagosol
* SobreHoteles_BicicletasAcuaticasLagosol
    - info_kb

## SobreHoteles_CaminatasLagosol
* SobreHoteles_CaminatasLagosol
    - info_kb

## Pasadia_HorarioTransporteLagosol
* Pasadia_HorarioTransporteLagosol
    - info_kb

## Pasadia_PrecioTransporteLagosol
* Pasadia_PrecioTransporteLagosol
    - info_kb

## Pasadia_CondicionesRestriccionesTransporte
* Pasadia_CondicionesRestriccionesTransporte
    - info_kb

## Pasadia_TransporteNiños
* Pasadia_TransporteNiños
    - info_kb

## Pasadia_PasadiaLagosol
* Pasadia_PasadiaLagosol
    - info_kb

## Pasadia_RecomendacionesPasadia
* Pasadia_RecomendacionesPasadia
    - info_kb

## Tarifas_TarifasPasadiaNoAfiliados
* Tarifas_TarifasPasadiaNoAfiliados
    - info_kb

## Tarifas_TarifasLagomar
* Tarifas_TarifasLagomar
    - info_kb


## Tarifas_TarifasLagosol
* Tarifas_TarifasLagosol
    - info_kb

## Convenios_CajaSinFronteras
* Convenios_CajaSinFronteras
    - info_kb

## Convenios_HotelesEstelar
* Convenios_HotelesEstelar
    - info_kb

## CreditoTurismo_SobreCreditoTurismo
* CreditoTurismo_SobreCreditoTurismo
    - info_kb

## CreditoTurismo_BeneficiosCreditoTurismo
* CreditoTurismo_BeneficiosCreditoTurismo
    - info_kb

## CreditoTurismo_RequisitosCreditoTurismo
* CreditoTurismo_RequisitosCreditoTurismo
    - info_kb   
## CreditoTurismo_DocumentosCreditoTurismo
* CreditoTurismo_DocumentosCreditoTurismo
    - info_kb



## CreditoTurismo_RealizarSolicitudCcredito
* CreditoTurismo_RealizarSolicitudCcredito
    - info_kb

## CreditoTurismo_TiempoAprobacionCreditoTurismo
* CreditoTurismo_TiempoAprobacionCreditoTurismo
    - info_kb

## CreditoTurismo_LibranzaCreditoTurismo
* CreditoTurismo_LibranzaCreditoTurismo
    - info_kb

## CreditoTurismo_AgenciaViajes
* CreditoTurismo_AgenciaViajes
    - info_kb

## Costo_Pasadia
* Costo_pasadia
    - info_kb


## Generated Story -2692820895129523724
* Afiliaciones_ComoPuedoAfiliarBeneficiarios
    - info_kb

## Generated Story -3089210366696181799
* AlimentosBebidas_VendenBebidasAlcoholicas
    - info_kb
* AlimentosBebidas_VendenBebidasAlcoholicas
    - info_kb

## Generated Story -9022285728475000469
* Reservas_Requisitos_RealizarReserva
    - info_kb
* Reservas_Requisitos_RealizarReserva
    - info_kb

## Generated Story -5813679302389261403
* Reservas_DisponibilidadTarifas
    - info_kb
## Generated Story 4271817488848395326
* Reservas_DisponibilidadTarifas
    - info_kb
## Generated Story 7752799187971381486
* SobreHoteles_Lavanderia
    - info_kb
* SobreHoteles_Lavanderia
    - info_kb

## Generated Story -7793388765489924396
* SobreHoteles_CaminatasLagosol
    - info_kb


## Generated Story 4561285893190552784
* CreditoTurismo_RequisitosCreditoTurismo
    - info_kb
## Generated Story -6108304547431680992
* Afiliaciones_AsignacionCategoria
    - info_kb

## Generated Story -6108304547431680992
* Afiliaciones_AsignacionCategoria
    - info_kb
## Generated Story 3083277019836424203
* Afiliaciones_AsignacionCategoria
    - info_kb
* Afiliaciones_AsignacionCategoria
    - info_kb

## Generated Story 540466414603380896
* Afiliaciones_ConocerCategoria
    - info_kb
* AlimentosBebidas_PlanesIncluyenBebidas
    - info_kb
* Canal_HorarioChat
    - info_kb
* Reservas_DisponibilidadTarifas
    - info_kb



## Info_lagomar
* Info_Lagomar
    - info_kb

## Info_lagosol
* Info_Lagosol
    - info_kb

## Info_Pasadia
* Info_Pasadia
    - info_kb




## Info_Hoteles
* Info_Hoteles
    - info_kb


## Que_Quiere_Saber
* Afirma
    - info_kb

## Agradece
*Agradece
    - info_kb

## Que_Quiere_Saber+Agradece
* Afirma+Agradece
    - info_kb

## action_TarifasPasadiaPorCategoria
* TarifasPasadiaPorCategoria
    - action_TarifasPasadiaPorCategoria

## Comida_Lagomar
* Comida_Lagomar
    - info_kb

## Comida_Lagosol
* Comida_Lagosol
    - info_kb

## Lagomar_Servicios
* Lagomar_Servicios
    - info_kb

## Lagomar_Planes
* Lagomar_Planes
    - action_PlanesLagomar
* Agradece
    - utter_PlanesTransferencia

## Lagomar_Planes
* Lagomar_Planes
    - action_PlanesLagomar
* Afirma
    - utter_PlanesTransferencia


## Lagomar_Planes
* Lagomar_Planes
    - action_PlanesLagomar
* Afirma+Agradece
    - utter_PlanesTransferencia

## Lagomar_Planes
* Lagomar_Planes
    - action_PlanesLagomar
* niega
    - utter_OfreceMasAyuda


## Lagomar_Eventos
* Lagomar_Eventos
    - info_kb

## Lagosol_Servicios
* Lagosol_Servicios
    - info_kb

## Lagosol_Planes
* Lagosol_Planes
    - action_PlanesLagosol
* Afirma
    - utter_PlanesTransferencia 

## Lagosol_Planes
* Lagosol_Planes
    - action_PlanesLagosol
* Afirma+Agradece
    - utter_PlanesTransferencia 

## Lagosol_Planes
* Lagosol_Planes
    - action_PlanesLagosol
* Agradece
    - utter_PlanesTransferencia

## Lagosol_Planes
* Lagosol_Planes
    - action_PlanesLagosol
* niega
    - utter_OfreceMasAyuda


## Lagosol_Eventos
* Lagosol_Eventos
    - info_kb

## Pasadia_Empresarial
* Pasadia_Empresarial
    - info_kb

## Pasadia_Horarios
* Pasadia_Horarios
    - info_kb


## tour_virtual
* tour_virtual
    - info_kb

## Plan_cumple
* Plan_cumple
    - info_kb

## Pagos_Tarjeta_Compensar
* Pagos_Tarjeta_Compensar
    - info_kb

## Pasadia_No_Afiliados
* Pasadia_No_Afiliados
    - info_kb

## Cambio_Fecha_Pasadia
* Cambio_Fecha_Pasadia
    - info_kb

## Reservar_Pagar_Cuotas
* Reservar_Pagar_Cuotas
    - info_kb

## Subsidio_Monetario
* Subsidio_Monetario
    - info_kb

## Pasadia_Servicio_Domestico
* Pasadia_Servicio_Domestico
    - info_kb

## Pasadia_Servicio_Domestico_Info
* Pasadia_Servicio_Domestico_Info
    - info_kb

## Disponibilidad_Pasadia
* Disponibilidad_Pasadia
    - info_kb

## Agua_Caliente
* Agua_Caliente
    - info_kb


## Hoteles_Reapertura
* Hoteles_Reapertura
    - info_kb

## Hoteles_MedidasHigiene
* Hoteles_MedidasHigiene
    - info_kb

## Hoteles_TarifaFinAño
* Hoteles_TarifaFinAño
    - info_kb

## Aplazar_Reserva
* Aplazar_Reserva
    - info_kb


## Protocolos_tapaboca
* Protocolos_tapaboca
    - info_kb

## Protocolos_distancia_social
* Protocolos_distancia_social
    - info_kb

## Protocolos_arreglo_habitacion
* Protocolos_arreglo_habitacion
    - info_kb

## Protocolos_actividad_recreativa
* Protocolos_actividad_recreativa
    - info_kb

## Protocolos_sintomas_covid
* Protocolos_sintomas_covid
    - info_kb

## Protocolos_medios_de_pago
* Protocolos_medios_de_pago
    - info_kb
## Protocolos_tapaboca
* Protocolos_tapaboca
    - info_kb

## Protocolos_distancia_social
* Protocolos_distancia_social
    - info_kb

## Protocolos_arreglo_habitacion
* Protocolos_arreglo_habitacion
    - info_kb

## Protocolos_actividad_recreativa
* Protocolos_actividad_recreativa
    - info_kb

## Protocolos_sintomas_covid
* Protocolos_sintomas_covid
    - info_kb

## Protocolos_medios_de_pago
* Protocolos_medios_de_pago
    - info_kb
