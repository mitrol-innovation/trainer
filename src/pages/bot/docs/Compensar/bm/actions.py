import json
import arrow
import requests
import warnings
import os
import pytz
from pydash import py_
from datetime import datetime
from typing import Any, Text, Dict, List, Union, Optional
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet
from rasa_sdk.forms import FormAction
from pytz import timezone
import calendar
from datetime import date
from datetime import datetime, timedelta
import numpy as np
import sys
import time
import csv




'''
Environment configuration
'''

ducklingUri = os.getenv('DUCKLING_URI')
ducklingTimezone = os.getenv('DUCKLING_TIMEZONE')
ducklingLocale = os.getenv('DUCKLING_LOCALE')
kbsUri = os.getenv('KBS_ADDRESS')


try:
        ducklingUri
except NameError:
        ducklingUri = None
if ducklingUri is None:
        ducklingUri = 'http://localhost:8888'
        print('DUCKLING ADDRESS',  ducklingUri)
else:
        print('DUCKLING ADDRESS',  ducklingUri)

try:
        ducklingTimezone
except NameError:
        ducklingTimezone = None
if ducklingTimezone is None:
        ducklingTimezone = 'America/Bogota'
        print('DUCKLING TIMEZONE',  ducklingTimezone)
else:
        print('DUCKLING TIMEZONE',  ducklingTimezone)


try:
        ducklingLocale
except NameError:
        ducklingLocale = None

if ducklingLocale is None:
        ducklingLocale = 'es_ES'
        print('DUCKLING LOCALE',  ducklingLocale)
else:
        print('DUCKLING LOCALE',  ducklingLocale)


try:
        kbsUri
except NameError:
        kbsUri = None
if kbsUri is None:
        kbsUri = 'http://localhost:6005'
        print('KBS URI',  kbsUri)
else:
        print('KBS URI',  kbsUri)


# mande el action_Greet al kbs


class bcolors:
    HEADER = '\033[95m'
    INFO = '\033[0;37;44m INFO \033[0m'
    WARNING = '\033[0;37;43m WARNING \033[0m'
    ERROR = '\033[0;37;41m ERROR \033[0m'
    ORIGIN = '\033[3;34;40m'
    OBJECT = '\033[1;35;40m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


'''
Exported modules
'''


def info(moduleName, message, value):
        print('\n' + arrow.now().format('YYYY-MM-DD HH:mm:ss') + ' ' + bcolors.INFO +
              bcolors.ORIGIN + '     ' + moduleName + '     ' + bcolors.ENDC + message)
        print(value)


'''
Config vars
'''
ducklingFormat = "YYYY-MM-DD[T]HH:mm:ss[.]SSSZZ"


'''
Aux functions
'''


def keys_exists(element, *keys):
    '''
    Check if *keys (nested) exists in `element` (dict).
    '''
    if not isinstance(element, dict):
        raise AttributeError('keys_exists() expects dict as first argument.')
    if len(keys) == 0:
        raise AttributeError(
            'keys_exists() expects at least two arguments, one given.')

    _element = element
    for key in keys:
        try:
            _element = _element[key]
        except KeyError:
            return False
    return True


def echo(val):
  print('VALUE : {}'.format(val))


def filterFutureDatesLimited1Year(val):
  response = {'valid': False}
  now = arrow.utcnow()
  limit = now.shift(years=+1)

  # It's an interval
  if keys_exists(val, 'to') and keys_exists(val, 'from'):
    # start = arrow.get(val['from']['value'])
    start = arrow.get(val['from']['value'], ducklingFormat).shift(hours=+15)
    # end = arrow.get(val['from']['value'], ducklingFormat).shift(hours=+15)
    end = arrow.get(val['to']['value'], ducklingFormat).shift(hours=-14)

    # end = arrow.get(val['to']['value'])
    if arrow.get(start).is_between(now, limit) and arrow.get(end).is_between(now, limit):
      return {'valid': True, 'from': start, 'to': end}
    else:
      print('invalid date')
  # It's a single
  elif keys_exists(val, 'value'):
    single = arrow.get(val['value'], ducklingFormat).shift(hours=+15)
    if arrow.get(single).is_between(now, limit):
      return {'valid': True, 'value': single}
    else:
      print('invalid date')


def testFilter(val):
  return {'filtered': True}


'''
Exported modules
'''


def parseDate(uri, locale, tz, text, allowPastDate, allowFutureDate, allowDuration):
    print("parsing {}".format(text))
    if allowFutureDate:
      print('future dates are allowed')
    if allowPastDate:
      print('past dates are allowed')

    '''
    Duckling aux vars initialization
    '''
    isUnderstood = False
    isDuration = False
    isSingleDate = False
    isMultiple = False

    '''
    Post data to duckling server
    '''
    parameters = {'locale': locale, 'tz': tz, 'text': text}
    r = requests.post(uri + '/parse', data=parameters)
    res = r.json()

    '''
    Response handling if duration and only future dates are allowed
    '''
    if allowDuration and allowFutureDate and not allowPastDate:
      if (len(res) == 0):
        print('not unterstood by duckling')
        return {'type': 'ERROR', 'value': text, 'needsDisambiguation': True, 'grain': None}
      elif (len(res) > 1 and (res[0]['dim']!='time') and (res[1]['dim']!='time')):
        return {'type': 'ERROR', 'value': text, 'needsDisambiguation': True, 'grain': None}
      elif len(res) > 1:  
        print('no duration, but multiple dates found')
        isMultiple = True
        isUnderstood = True
        results = res[1]['value']['values']
        print(results)
        firstResultList = res[1]['value']['values'][0]
        isSingleDate = keys_exists(firstResultList, 'grain')
        if isSingleDate:
          print('**** SINGLE DATE ****')
          filtered = py_(results).for_each(echo).map(
              filterFutureDatesLimited1Year)
          response = py_.compact(filtered.value())
          print(response)
          print(">>> future dates: {}".format(response))
          if len(response) == 0:
            return {'type': 'ERROR', 'value': None, 'needsDisambiguation': True, 'grain': None}
          else:
            value = response[0]['value']
            return {'type': 'SINGLE', 'value': value, 'needsDisambiguation': False, 'grain': None}
        else:
          print('**** UNKNOWN ERROR *****')
          return {'type': 'ERROR', 'value': None, 'needsDisambiguation': False, 'grain': 'GRAIN'}

        #isInterval = keys_exists(firstResultList, 'to')
        #isSingleDate = keys_exists(firstResultList, 'grain')

        #print(res[1]['value']['values'][0]['value'])
        #return {'type': 'MULTIPLE', 'value': text,  'needsDisambiguation': True, 'grain': None}

      elif len(res) == 1:
        print('single option group found by duckling')
        isUnderstood = True
        results = res[0]['value']['values']
        firstResultList = res[0]['value']['values'][0]
        isInterval = keys_exists(firstResultList, 'to')
        isSingleDate = keys_exists(firstResultList, 'grain')
        arri = []

        if isInterval:
          print('**** INTERVAL ****')
          # for batch in firstResultList:
          #   print (batch)
          filtered = py_(results).for_each(echo).map(
              filterFutureDatesLimited1Year)
          response = py_.compact(filtered.value())
          # froms = py_(results).for_each(echo).map(testFilter)
          print(">>> future dates: {}".format(response))

          if len(response) == 0:
            return {'type': 'ERROR', 'value': None, 'needsDisambiguation': True, 'grain': None}
          else:
            start = response[0]['from']
            end = response[0]['to']
            return {'type': 'INTERVAL', 'value': {'from': start, 'to': end}, 'needsDisambiguation': False, 'grain': 'GRAIN'}

        elif isSingleDate:
          print('**** SINGLE DATE ****')
          filtered = py_(results).for_each(echo).map(
              filterFutureDatesLimited1Year)
          response = py_.compact(filtered.value())
          print(response)
          print(">>> future dates: {}".format(response))
          if len(response) == 0:
            return {'type': 'ERROR', 'value': None, 'needsDisambiguation': True, 'grain': None}
          else:
            value = response[0]['value']
            return {'type': 'SINGLE', 'value': value, 'needsDisambiguation': False, 'grain': None}
        else:
          print('**** UNKNOWN ERROR *****')
          return {'type': 'ERROR', 'value': None, 'needsDisambiguation': False, 'grain': 'GRAIN'}
    # '''
    #   # TODO build code to allow past dates and ban intervals
    # '''


def dayToNight(dayStr) -> Text:
        # if "0" in dayStr:
        #         return str(dayStr.replace('0 días', '1 noche')).upper()
        if "días" in dayStr:
                return str(dayStr.replace('días', 'noches')).upper()
        else:
                return str(dayStr.replace('día', 'noche')).upper()


class bcolors:
    HEADER = '\033[95m'
    INFO = '\033[0;37;44m INFO \033[0m'
    WARNING = '\033[0;37;43m WARNING \033[0m'
    ERROR = '\033[0;37;41m ERROR \033[0m'
    ORIGIN = '\033[3;34;40m'
    OBJECT = '\033[1;35;40m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class ReservationForm(FormAction):
#
        def name(self) -> Text:
                return "reservation_form"

        @staticmethod
        def required_slots(tracker: Tracker) -> List[Text]:
                return ["reserva_hotel", "reserva_fecha_check_in", "reserva_fecha_check_out"]

        def validate_reserva_fecha_check_in(self,
                value: Text,
                dispatcher: CollectingDispatcher,
                tracker: Tracker,
                domain: Dict[Text, Any],
                ) -> Optional[Text]:
                # past dates are not allowed
                parsed = parseDate(ducklingUri, ducklingLocale,
                                   ducklingTimezone, value, False, True, True)
                if parsed['needsDisambiguation']:
                  dispatcher.utter_message(
                      'Lo siento, no puedo entender esa fecha, podría intentarlo de nuevo por favor?')
                  print('type: {}, parsed date : {}, needs disambiguation'.format(
                      parsed['type'], parsed['value']))
                  return {'reserva_fecha_check_in': None}
                else:
                  if parsed['type'] == 'INTERVAL':
                    return {
                      'reserva_fecha_check_in': arrow.get(parsed['value']['from']).format('dddd, DD.MMMM.YYYY', locale='es_es'),
                      'reserva_fecha_check_out': arrow.get(parsed['value']['to']).format('dddd, DD.MMMM.YYYY', locale='es_es')
                    }
                  elif parsed['type'] == 'SINGLE':
                    return {
                      'reserva_fecha_check_in': arrow.get(parsed['value']).format('dddd, DD.MMMM.YYYY', locale='es_es')
                    }
                  print('type: {}, parsed date : {}, no need for disambiguation'.format(
                      parsed['type'], parsed['value']))

        def validate_reserva_fecha_check_out(self,
                value: Text,
                dispatcher: CollectingDispatcher,
                tracker: Tracker,
                domain: Dict[Text, Any],
                ) -> Optional[Text]:
                # past dates are not allowed
                parsed = parseDate(ducklingUri, ducklingLocale,
                                   ducklingTimezone, value, False, True, True)
                print(parsed)
                if parsed['needsDisambiguation']:
                  dispatcher.utter_message(
                      'Lo siento, no puedo entender esa fecha, podría intentarlo de nuevo por favor?')
                  print('type: {}, parsed date : {}, needs disambiguation'.format(
                      parsed['type'], parsed['value']))
                  return {'reserva_fecha_check_out': None}
                else:
                  if parsed['type'] == 'INTERVAL':
                    return {
                      'reserva_fecha_check_in': arrow.get(parsed['value']['from']).format('dddd, DD.MMMM.YYYY', locale='es_es'),
                      'reserva_fecha_check_out': arrow.get(parsed['value']['to']).format('dddd, DD.MMMM.YYYY', locale='es_es'),
                    }
                  elif parsed['type'] == 'SINGLE':
                    return {
                      'reserva_fecha_check_out': arrow.get(parsed['value']).format('dddd, DD.MMMM.YYYY', locale='es_es'),
                    }
                  print('type: {}, parsed date : {}, no need for disambiguation'.format(
                      parsed['type'], parsed['value']))

        def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict]]]:
                return {
                        "reserva_fecha_check_in": self.from_entity(entity="time"),
                        "reserva_fecha_check_out": self.from_entity(entity="time"),
                        "reserva_hotel": self.from_entity(entity="hotel")
                }

        def submit(
                self,
                dispatcher: CollectingDispatcher,
                tracker: Tracker,
                domain: Dict[Text, Any],
        ) -> List[Dict]:
                info('ReservationForm', 'submitted', '')
                print('\n' + arrow.now().format('YYYY-MM-DD HH:MM:ss') + bcolors.INFO +
                      bcolors.ORIGIN + '   ReservationForm   ' + bcolors.ENDC + ' submitted')
                # dispatcher.utter_template("utter_reservation_form_submitted", tracker)
                return []





class DisponibilidadTarifas(FormAction):
#
        def name(self) -> Text:
                return "DisponibilidadTarifas_form"

        @staticmethod
        def required_slots(tracker: Tracker) -> List[Text]:
                return ["DisponibilidadTarifas", "reserva_fecha_check_in", "reserva_fecha_check_out"]

        def validate_reserva_fecha_check_in(self,
                value: Text,
                dispatcher: CollectingDispatcher,
                tracker: Tracker,
                domain: Dict[Text, Any],
                ) -> Optional[Text]:
                # past dates are not allowed
                  parsed = parseDate(ducklingUri, ducklingLocale,
                                    ducklingTimezone, value, False, True, True)
                  if parsed['needsDisambiguation']:
                    dispatcher.utter_message(
                        'Lo siento, no puedo entender esa fecha, podría intentarlo de nuevo por favor?')
                    print('type: {}, parsed date : {}, needs disambiguation'.format(
                        parsed['type'], parsed['value']))
                    return {'reserva_fecha_check_in': None}
                  else:
                    if parsed['type'] == 'INTERVAL':
                      return {
                        'reserva_fecha_check_in': arrow.get(parsed['value']['from']).format('dddd, DD.MMMM.YYYY', locale='es_es'),
                        'reserva_fecha_check_out': arrow.get(parsed['value']['to']).format('dddd, DD.MMMM.YYYY', locale='es_es')
                      }
                    elif parsed['type'] == 'SINGLE':
                      return {
                        'reserva_fecha_check_in': arrow.get(parsed['value']).format('dddd, DD.MMMM.YYYY', locale='es_es')
                      }
                    print('type: {}, parsed date : {}, no need for disambiguation'.format(
                        parsed['type'], parsed['value']))

        def validate_reserva_fecha_check_out(self,
                value: Text,
                dispatcher: CollectingDispatcher,
                tracker: Tracker,
                domain: Dict[Text, Any],
                ) -> Optional[Text]:
                # past dates are not allowed
                parsed = parseDate(ducklingUri, ducklingLocale,
                                   ducklingTimezone, value, False, True, True)
                print(parsed)
                if parsed['needsDisambiguation']:
                  dispatcher.utter_message(
                      'Lo siento, no puedo entender esa fecha, podría intentarlo de nuevo por favor?')
                  print('type: {}, parsed date : {}, needs disambiguation'.format(
                      parsed['type'], parsed['value']))
                  return {'reserva_fecha_check_out': None}
                else:
                  if parsed['type'] == 'INTERVAL':
                    return {
                      'reserva_fecha_check_in': arrow.get(parsed['value']['from']).format('dddd, DD.MMMM.YYYY', locale='es_es'),
                      'reserva_fecha_check_out': arrow.get(parsed['value']['to']).format('dddd, DD.MMMM.YYYY', locale='es_es'),
                    }
                  elif parsed['type'] == 'SINGLE':
                    return {
                      'reserva_fecha_check_out': arrow.get(parsed['value']).format('dddd, DD.MMMM.YYYY', locale='es_es'),
                    }
                  print('type: {}, parsed date : {}, no need for disambiguation'.format(
                      parsed['type'], parsed['value']))

        def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict]]]:
                return {
                        "reserva_fecha_check_in": self.from_entity(entity="time"),
                        "reserva_fecha_check_out": self.from_entity(entity="time"),
                        "DisponibilidadTarifas": self.from_entity(entity="hotel")
                }

        def submit(
                self,
                dispatcher: CollectingDispatcher,
                tracker: Tracker,
                domain: Dict[Text, Any],
        ) -> List[Dict]:
                info('ReservationForm', 'submitted', '')
                print('\n' + arrow.now().format('YYYY-MM-DD HH:MM:ss') + bcolors.INFO +
                      bcolors.ORIGIN + '   ReservationForm   ' + bcolors.ENDC + ' submitted')
                # dispatcher.utter_template("utter_reservation_form_submitted", tracker)
                return []


class ActionLlamado(Action):

    def name(self) -> Text:
        return "action_llamado"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        message = tracker.latest_message['text'] 
        payload = {"sender": tracker.sender_id, "message": message}
        r = requests.post(kbsUri + "/webhooks/rest/webhook", json=payload)
        response_dict = json.loads(r.text)
        dispatcher.utter_message(response_dict[0]['text'])

        # dispatcher.utter_message('action_llamado ok')

        return []




class ActionDefaultMitrol(Action):

    def name(self) -> Text:
        return "action_default_mitrol"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        # print(json.loads(vars(tracker)))
        now_colombia = datetime.now(timezone('America/Bogota'))
        UltimaHora= now_colombia.strftime("%Y-%m-%d %H:%M:%S %Z%z")
        Colombia7am = now_colombia.replace(hour=7, minute=00, second=0, microsecond=0)
        Colombia8pm = now_colombia.replace(hour=20, minute=00, second=0, microsecond=0)
        Colombia5pm = now_colombia.replace(hour=17, minute=00, second=0, microsecond=0)
        Colombia6pm = now_colombia.replace(hour=18, minute=00, second=0, microsecond=0)
        Colombia10am = now_colombia.replace(hour=10, minute=00, second=0, microsecond=0)
        Colombia8am = now_colombia.replace(hour=8, minute=00, second=0, microsecond=0)
        daysLaV = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]
        daysS = ["Saturday"]
        daysD = ["Sunday"]
        dia = calendar.day_name[date.today().weekday()]
        dia_calendario=date.today()
        feriados_colombia=[date(2020,3,23),date(2020,4,5),date(2020,4,9),date(2020,4,10),date(2020,4,12),
                            date(2020,5,1),date(2020,5,25),date(2020,6,15),date(2020,6,22),date(2020,6,29),
                            date(2020,7,20),date(2020,8,7),date(2020,8,17),date(2020,10,12),date(2020,11,2),
                            date(2020,11,16),date(2020,12,8),date(2020,12,25),date(2021,1,1),date(2021,1,11),
                            date(2021,3,22),date(2021,3,28),date(2021,4,1),date(2021,4,2),date(2021,4,4),
                            date(2021,5,1),date(2021,5,17),date(2021,6,7),date(2021,6,14),date(2021,7,5),
                            date(2021,7,20),date(2021,8,7),date(2021,8,16),date(2021,10,18),date(2021,11,1),
                            date(2021,11,15),date(2021,12,8),date(2021,12,25)]  
        trackerDumped = json.dumps(vars(tracker))
        trackerDict = json.loads(trackerDumped)
        confidence = []
        for i in range(0, len(trackerDict['events'])):
            if trackerDict['events'][i]['event'] == 'user':
                confidence.append(
                    trackerDict['events'][i]['parse_data']['intent']['confidence'])
        t = list(np.where(np.array(confidence) < 0.7)[0])
        for i in range(0, len(t)):
            if i % 2 == 0:
                confidence[t[i]] = -1
            else:
                confidence[t[i]] = -2
  
        intent_index=[]
        with open(r'/app/actions/respuestas_bot_alojamiento.csv', encoding='utf-8') as csvfile:
          reader=csv.reader(csvfile)
          for row in reader:
            intent_index.append(row)
        intent_index=np.array(intent_index)
        t=np.where(intent_index[:,0]=="Default_Transferencia_En_Horario")[0][0]
        tf=np.where(intent_index[:,0]=="Default_Transferencia_Fuera_De_Horario")[0][0]
        d1=np.where(intent_index[:,0]=="Default_1")[0][0]
        d2=np.where(intent_index[:,0]=="Default_2")[0][0]

        message_Default_Transferencia_En_Horario=intent_index[t,1]
        message_Default_Transferencia_Fuera_De_Horario=intent_index[tf,1]
        message_Default_1=intent_index[d1,1]
        message_Default_2=intent_index[d2,1]


        if (dia in daysD) or (dia_calendario in feriados_colombia):
                if Colombia10am < now_colombia < Colombia6pm:
                        for i in range(len(confidence)-1,len(confidence)-3,-1):
                                if (i>0 and confidence[i-2]==-1 and confidence[i-1]==-2 and confidence[i]==-1) or (i>0 and confidence[i-2]==-2 and confidence[i-1]==-1 and confidence[i]==-2):
                                        dispatcher.utter_message(message_Default_Transferencia_En_Horario)
                                        return [SlotSet('UltimaHora',UltimaHora)]
                                if i>0 and confidence[i]==-1 or confidence[0]==-1:
                                        dispatcher.utter_message(message_Default_1)
                                        break
                                if i>0 and confidence[i]==-2:
                                        dispatcher.utter_message(message_Default_2)
                                        break

                        return []
                else:
                        for i in range(len(confidence)-1,len(confidence)-3,-1):
                                if (i>0 and confidence[i-2]==-1 and confidence[i-1]==-2 and confidence[i]==-1) or (i>0 and confidence[i-2]==-2 and confidence[i-1]==-1 and confidence[i]==-2):
                                        dispatcher.utter_message(message_Default_Transferencia_Fuera_De_Horario)
                                        return [SlotSet('UltimaHora',UltimaHora)]
                                if i>0 and confidence[i]==-1 or confidence[0]==-1:
                                        dispatcher.utter_message(message_Default_1)
                                        break
                                if i>0 and confidence[i]==-2:
                                        dispatcher.utter_message(message_Default_2)
                                        break
                        return[]
        if dia in daysLaV:
            if Colombia7am < now_colombia < Colombia8pm:
                for i in range(len(confidence)-1, len(confidence)-3, -1):
                    if (i > 0 and confidence[i-2] == -1 and confidence[i-1] == -2 and confidence[i] == -1) or (i > 0 and confidence[i-2] == -2 and confidence[i-1] == -1 and confidence[i] == -2):
                        dispatcher.utter_message(message_Default_Transferencia_En_Horario)
                        return [SlotSet('UltimaHora',UltimaHora)]
                    elif i > 0 and confidence[i] == -1 or confidence[0] == -1:
                        dispatcher.utter_message(message_Default_1)
                        break
                    elif i > 0 and confidence[i] == -2:
                        dispatcher.utter_message(message_Default_2)
                        break
                    else:
                        break
                return []
            else:
                for i in range(len(confidence)-1,len(confidence)-3,-1):
                    if (i>0 and confidence[i-2]==-1 and confidence[i-1]==-2 and confidence[i]==-1) or (i>0 and confidence[i-2]==-2 and confidence[i-1]==-1 and confidence[i]==-2):
                        dispatcher.utter_message(message_Default_Transferencia_Fuera_De_Horario)
                        return [SlotSet('UltimaHora',UltimaHora)]
                    if i>0 and confidence[i]==-1 or confidence[0]==-1:
                        dispatcher.utter_message(message_Default_1)
                        break
                    if i>0 and confidence[i]==-2:
                        dispatcher.utter_message(message_Default_2)
                        break
                return[]

        elif dia in daysS:
            if Colombia8am < now_colombia < Colombia5pm:
                for i in range(len(confidence)-1,len(confidence)-3,-1):
                    if (i>0 and confidence[i-2]==-1 and confidence[i-1]==-2 and confidence[i]==-1) or (i>0 and confidence[i-2]==-2 and confidence[i-1]==-1 and confidence[i]==-2):
                        dispatcher.utter_message(message_Default_Transferencia_En_Horario)
                        return [SlotSet('UltimaHora',UltimaHora)]
                    if i>0 and confidence[i]==-1 or confidence[0]==-1:
                        dispatcher.utter_message(message_Default_1)
                        break
                    if i>0 and confidence[i]==-2:
                        dispatcher.utter_message(message_Default_2)
                        break
                return []

            else:
                for i in range(len(confidence)-1,len(confidence)-3,-1):
                    if (i>0 and confidence[i-2]==-1 and confidence[i-1]==-2 and confidence[i]==-1) or (i>0 and confidence[i-2]==-2 and confidence[i-1]==-1 and confidence[i]==-2):
                        dispatcher.utter_message(message_Default_Transferencia_Fuera_De_Horario)
                        return [SlotSet('UltimaHora',UltimaHora)]
                    if i>0 and confidence[i]==-1 or confidence[0]==-1:
                        dispatcher.utter_message(message_Default_1)
                        break
                    if i>0 and confidence[i]==-2:
                        dispatcher.utter_message(message_Default_2)
                        break
                return[]





class ActionPasameAgente(Action):
    def name(self):
        return "action_transfer_reserva"

    def run(self, dispatcher, tracker, domain):
        intent_index=[]
        with open(r'/app/actions/respuestas_bot_alojamiento.csv', encoding='utf-8') as csvfile:
            reader=csv.reader(csvfile)
            for row in reader:
                intent_index.append(row)
        intent_index=np.array(intent_index)

        k=np.where(intent_index[:,0]=="Transfer_Reserva_En_Horario")[0][0]
        j=np.where(intent_index[:,0]=="Transfer_Reserva_Fuera_De_Horario")[0][0]
        message_Transfer_Reserva_En_Horario=intent_index[k,1]
        message_Transfer_Reserva_Fuera_De_Horario=intent_index[j,1]
        now_colombia = datetime.now(timezone('America/Bogota'))
        UltimaHora= now_colombia.strftime("%Y-%m-%d %H:%M:%S %Z%z")
        Colombia7am = now_colombia.replace(hour=7, minute=00, second=0, microsecond=0)
        Colombia8pm = now_colombia.replace(hour=20, minute=00, second=0, microsecond=0)
        Colombia5pm = now_colombia.replace(hour=17, minute=00, second=0, microsecond=0)
        Colombia6pm = now_colombia.replace(hour=18, minute=00, second=0, microsecond=0)
        Colombia10am = now_colombia.replace(hour=10, minute=00, second=0, microsecond=0)
        Colombia8am = now_colombia.replace(hour=8, minute=00, second=0, microsecond=0)
        daysLaV = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]
        daysS = ["Saturday"]
        daysD = ["Sunday"]
        dia = calendar.day_name[date.today().weekday()]
        dia_calendario=date.today()
        feriados_colombia=[date(2020,3,23),date(2020,4,5),date(2020,4,9),date(2020,4,10),date(2020,4,12),
                            date(2020,5,1),date(2020,5,25),date(2020,6,15),date(2020,6,22),date(2020,6,29),
                            date(2020,7,20),date(2020,8,7),date(2020,8,17),date(2020,10,12),date(2020,11,2),
                            date(2020,11,16),date(2020,12,8),date(2020,12,25),date(2021,1,1),date(2021,1,11),
                            date(2021,3,22),date(2021,3,28),date(2021,4,1),date(2021,4,2),date(2021,4,4),
                            date(2021,5,1),date(2021,5,17),date(2021,6,7),date(2021,6,14),date(2021,7,5),
                            date(2021,7,20),date(2021,8,7),date(2021,8,16),date(2021,10,18),date(2021,11,1),
                            date(2021,11,15),date(2021,12,8),date(2021,12,25)]  

        if (dia in daysD) or (dia_calendario in feriados_colombia):
                if Colombia10am < now_colombia < Colombia6pm:
                    dispatcher.utter_message(message_Transfer_Reserva_En_Horario)
                else:
                    dispatcher.utter_message(message_Transfer_Reserva_Fuera_De_Horario)
        if dia in daysLaV:
                if Colombia7am < now_colombia < Colombia8pm:
                    dispatcher.utter_message(message_Transfer_Reserva_En_Horario)
                else:
                    dispatcher.utter_message(message_Transfer_Reserva_Fuera_De_Horario)
        if dia in daysS:
                if Colombia8am < now_colombia < Colombia5pm:
                    dispatcher.utter_message(message_Transfer_Reserva_En_Horario)
                else:
                    dispatcher.utter_message(message_Transfer_Reserva_Fuera_De_Horario)

        return [SlotSet('UltimaHora',UltimaHora)]

class PlanGeneral(Action):
    def name(self):
        return "action_PlanGeneral"

    def run(self, dispatcher, tracker, domain):
        intent_index=[]
        with open(r'/app/actions/respuestas_bot_alojamiento.csv', encoding='utf-8') as csvfile:
            reader=csv.reader(csvfile)
            for row in reader:
                intent_index.append(row)
        intent_index=np.array(intent_index)
        k=np.where(intent_index[:,0]=="PlanesGeneral")[0][0]
        j=np.where(intent_index[:,0]=="PlanesFueraHorario")[0][0]
        message_PlanesGeneral=intent_index[k,1]
        message_PlanesFueraHorario=intent_index[j,1]
        now_colombia = datetime.now(timezone('America/Bogota'))
        UltimaHora= now_colombia.strftime("%Y-%m-%d %H:%M:%S %Z%z")
        Colombia7am = now_colombia.replace(hour=7, minute=00, second=0, microsecond=0)
        Colombia8pm = now_colombia.replace(hour=20, minute=00, second=0, microsecond=0)
        Colombia5pm = now_colombia.replace(hour=17, minute=00, second=0, microsecond=0)
        Colombia6pm = now_colombia.replace(hour=18, minute=00, second=0, microsecond=0)
        Colombia10am = now_colombia.replace(hour=10, minute=00, second=0, microsecond=0)
        Colombia8am = now_colombia.replace(hour=8, minute=00, second=0, microsecond=0)
        daysLaV = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]
        daysS = ["Saturday"]
        daysD = ["Sunday"]
        dia = calendar.day_name[date.today().weekday()]
        dia_calendario=date.today()
        feriados_colombia=[date(2020,3,23),date(2020,4,5),date(2020,4,9),date(2020,4,10),date(2020,4,12),
                            date(2020,5,1),date(2020,5,25),date(2020,6,15),date(2020,6,22),date(2020,6,29),
                            date(2020,7,20),date(2020,8,7),date(2020,8,17),date(2020,10,12),date(2020,11,2),
                            date(2020,11,16),date(2020,12,8),date(2020,12,25),date(2021,1,1),date(2021,1,11),
                            date(2021,3,22),date(2021,3,28),date(2021,4,1),date(2021,4,2),date(2021,4,4),
                            date(2021,5,1),date(2021,5,17),date(2021,6,7),date(2021,6,14),date(2021,7,5),
                            date(2021,7,20),date(2021,8,7),date(2021,8,16),date(2021,10,18),date(2021,11,1),
                            date(2021,11,15),date(2021,12,8),date(2021,12,25)]

        if (dia in daysD) or (dia_calendario in feriados_colombia):
                if Colombia10am < now_colombia < Colombia6pm:
                    dispatcher.utter_message(message_PlanesGeneral)
                else:
                    dispatcher.utter_message(message_PlanesFueraHorario)
        if dia in daysLaV:
                if Colombia7am < now_colombia < Colombia8pm:
                    dispatcher.utter_message(message_PlanesGeneral)
                else:
                    dispatcher.utter_message(message_PlanesFueraHorario)
        if dia in daysS:
                if Colombia8am < now_colombia < Colombia5pm:
                    dispatcher.utter_message(message_PlanesGeneral)
                else:
                    dispatcher.utter_message(message_PlanesFueraHorario)


        return [SlotSet('UltimaHora',UltimaHora)]