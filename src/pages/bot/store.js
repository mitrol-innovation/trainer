import { mockNluBM } from './docs/mockBM.js'
import { mockNluKBS } from './docs/mockKBS.js'
import { mockRespuestas } from './docs/mockCSV.js'
import { Loading, QSpinnerGears, Notify } from 'quasar'
import { ENVIRONMENTS } from '../../environments'
import apiSrv from 'fwk-api'
import moment from 'moment'
import Vue from 'vue'
window.Vue = Vue


const state = {
    initOK: false,
    intentionsKBS: undefined,
    intentionsBM: undefined,
    selMode: undefined,
    selIntention: '',
    selIntentionIndex: -1,
    dirtyFlag: false
}
const getters = {}
const mutations = {
    setInitOK (state, flag) {
        state.initOK = flag
        console.log('store setInitOK', flag)
    },
    setSelMode (state, val) {
        state.selMode = val
        console.log('store setSelMode', val)
    },
    setIntNames (state, names) {
        state.intNames = names
        console.log('store setIntNames', names.length)
    },
    setIntentionsKBS (state, ints) {
        state.intentionsKBS = ints
        console.log('store setIntentionsKBS', ints.length)
    },
    setIntentionsBM (state, ints) {
        state.intentionsBM = ints
        console.log('store setIntentionsBM', ints.length)
    },
    setSelIntention (state, sel) {
        state.selIntention = sel
    },
    setDirtyFlag (state, flag) {
        // Vue.set(state, 'dirtyFlag', flag)
        state.dirtyFlag = flag
        console.log('STORE: dirtyFlag: ', state.dirtyFlag)
    },
    setDirty (state, payload) {
        state[payload.prop][payload.id].dirty = false
        state[payload.prop].dirty = false
    },
    setExample (state, payload) {
        console.log('STORE setExample:' + payload.id + ' -> ' + payload.text)
        state.selIntention.dirty = true
        state.selIntention.ejemplos[payload.id].text = payload.text
        state.selIntention.ejemplos[payload.id].dirty = true
    },
    setAnswer (state, payload) {
        console.log('STORE setAnswer:' + payload.rta)
        state.selIntention.dirty = true
        state.selIntention.respuesta.text = payload.rta
        state.selIntention.respuesta.dirty = true
    },
    setFile (state, data) {
        console.log('store setFile', data.length)
        const f = data.toString('utf8')
        state.dataFile = f
    },
    setRespuesta (state, data) {
        data.found.respuesta = {
            text: data.match.trim().slice(1, -1)
        }
    }
}
const actions = {
    initApiSrv () {
        const cfg = {
            url: ENVIRONMENTS.urlBase
        }
        apiSrv.init(cfg)
    },
    async loadMocks ({ commit, dispatch }) {
        commit('setSelMode', 'mockMode')

        dispatch('processKBS', mockNluKBS)
        dispatch('processBM', mockNluBM)
        dispatch('processCSV', mockRespuestas)
        commit('setInitOK', true)
    },
    async loadFiles ({ commit, dispatch }) {
        try {
            Loading.show({
                spinner: QSpinnerGears,
                spinnerColor: 'white',
                spinnerSize: 140,
                backgroundColor: 'gray',
                message: 'cargando...',
                messageColor: 'black'
            })
            commit('setSelMode', 'filesMode')

            const kbsData = await apiSrv.callSrv('GET', '/bot/file/kbs')
            const bmData = await apiSrv.callSrv('GET', '/bot/file/bm')
            const csvData = await apiSrv.callSrv('GET', '/bot/file/csv')

            dispatch('processKBS', kbsData)
            dispatch('processBM', bmData)
            dispatch('processCSV', csvData)
            commit('setInitOK', true)
        } catch (error) {
            console.log('error sendClient: ', error)
            Notify.create({
                message: error.message,
                color: 'red-12',
                textColor: 'white',
                icon: 'error'
            })
        } finally {
            Loading.hide()
        }
    },
    loadLocals ({ commit }) {
        commit('setSelMode', 'localsMode')
    },
    processFile ({dispatch}, payload) {
        switch (payload.fileType) {
        case 'KBS':
            dispatch('processKBS', payload.result)
            break
        case 'BM':
            dispatch('processBM',payload.result)
            break
        case 'CSV':
            dispatch('processCSV',payload.result)
            break

        default:
            break
        }
    },
    async saveExample ({ commit }, payload) {
        Loading.show({
            spinner: QSpinnerGears,
            spinnerColor: 'white',
            spinnerSize: 140,
            backgroundColor: 'gray',
            message: 'cargando...',
            messageColor: 'black'
        })
        commit('setExample', payload)
        Loading.hide()
    },
    async saveAnswer ({ commit }, text) {
        Loading.show({
            spinner: QSpinnerGears,
            spinnerColor: 'white',
            spinnerSize: 140,
            backgroundColor: 'gray',
            message: 'cargando...',
            messageColor: 'black'
        })
        commit('setAnswer', { rta: text })
        Loading.hide()
    },
    async downloadFiles ({ state }) {
        const now = moment().format('YYYYMMDD HHmm')
        const strKBS = generateNlu(state.intentionsKBS)
        const strBM = generateNlu(state.intentionsBM)
        const strCSV = generateAnswers(state.intentionsKBS)
        const intentionsCSV = generateIntentions(
            state.intentionsKBS,
            state.intentionsBM
        )

        switch (state.selMode) {
        case 'mockMode':
        case 'localsMode':
            download(strKBS, `nlu_bm_${now}.md`, 'markdown')
            download(strBM, `nlu_kbs_${now}.md`, 'markdown')
            download(strCSV, `respuestas_${now}.csv`, 'csv')
            download(intentionsCSV, `intenciones_${now}.csv`, 'csv')
            break
        case 'filesMode': {
            const bm = { bm: strBM }
            sendFiles(bm)
            const kbs = { kbs: strKBS }
            sendFiles(kbs)
            const csv = { csv: strCSV }
            sendFiles(csv)
        }
            break
        default:
            break
        }
    },
    processKBS ({ commit }, dataStr) {
        const intKBS = []
        dataStr = dataStr + '\n\n' // agrega carriage returns para match con regex
        dataStr = dataStr.replace(/<!--.*?-->/gs, '') // elimina comentarios
        dataStr = dataStr.toLowerCase() // solo minusculas

        const regexNLU = /(##(\sintent:(.+?))\n\n)/gms
        let reg = ''
        while ((reg = regexNLU.exec(dataStr)) !== null) {
            if (reg.index === regexNLU.lastIndex) {
                regexNLU.lastIndex++
            }
            let intencion = ''
            reg.forEach((match, groupIndex) => {
                if (groupIndex === 3) {
                    const info = match.split('- ') // La intencion es el primer match
                    intencion = info.shift().trim() // Eliminar acentos

                    const newInt = {
                        text: intencion,
                        ejemplos: [],
                        dirty: false
                    }
                    intKBS.push(newInt)
                    info.forEach(ej => {
                        newInt.ejemplos.push({
                            text: ej
                        })
                    })
                }
            })
        }
        commit('setIntentionsKBS', intKBS)
    },
    processBM ({ commit }, dataStr) {
        const intBM = []
        dataStr = dataStr.replace(/<!--.*?-->/gs, '')
        dataStr = dataStr.toLowerCase() // solo minusculas
        dataStr = removeAccents(dataStr)

        const regexNLU = /(##(\sintent:(.+?))\n\n)/gms
        let reg = ''
        while ((reg = regexNLU.exec(dataStr)) !== null) {
            if (reg.index === regexNLU.lastIndex) {
                regexNLU.lastIndex++
            }
            let intencion = ''
            reg.forEach((match, groupIndex) => {
                if (groupIndex === 3) {
                    const info = match.split('- ') // La intencion es el primer match
                    intencion = info.shift().trim()

                    const newInt = {
                        text: intencion,
                        ejemplos: [],
                        dirty: false
                    }
                    intBM.push(newInt)
                    info.forEach(ej => {
                        newInt.ejemplos.push({
                            text: ej
                        })
                    })
                }
            })
        }
        commit('setIntentionsBM', intBM)
    },
    processCSV ({ commit, state }, dataStr) {
        const regexCSV = /(.+?,)(".+?")/gms
        let foundKBSCounter = 0
        let foundBMCounter = 0
        let notFoundIntCounter = 0
        let mr = ''
        while ((mr = regexCSV.exec(dataStr)) !== null) {
            if (mr.index === regexCSV.lastIndex) {
                regexCSV.lastIndex++
            }
            let intencion = ''
            // eslint-disable-next-line no-loop-func
            mr.forEach((match, groupIndex) => {
                if (groupIndex === 1) {
                    intencion = match.trim().slice(0, -1).
                        toLowerCase()
                }

                if (groupIndex === 2) {
                    const foundKBS = state.intentionsKBS.find(x => x.text === intencion)
                    if (foundKBS) {
                        foundKBSCounter++
                        commit('setRespuesta', { found: foundKBS, match })
                    }

                    const foundBM = state.intentionsBM.find(x => x.text === intencion)
                    if (foundBM) {
                        foundBMCounter++
                        commit('setRespuesta', { found: foundBM, match })
                    }

                    if (!foundKBS && !foundBM) {
                        notFoundIntCounter++
                    }
                }
            })
        }
        console.log('Resp.Found KBS: ', foundKBSCounter)
        console.log('Resp.Found BM: ', foundBMCounter)
        console.log('Not Found Ints: ', notFoundIntCounter)
    }
}
export default {
    // eslint-disable-next-line no-undef
    strict: process.env.NODE_ENV !== 'production',
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}

async function sendFiles (pl) {
    try {
        Loading.show({
            spinner: QSpinnerGears,
            spinnerColor: 'white',
            spinnerSize: 140,
            backgroundColor: 'gray',
            message: 'cargando...',
            messageColor: 'black'
        })
        const res = await apiSrv.callSrv('POST', '/bot/file', pl)
        Notify.create({
            message: 'Se han guardado los archivos!',
            color: 'green-6',
            textColor: 'white',
            icon: 'error'
        })
        return res
    } catch (error) {
        console.log('error sendClient: ', error)
        Notify.create({
            message: error.message,
            color: 'red-12',
            textColor: 'white',
            icon: 'error'
        })
        return false
    } finally {
        Loading.hide()
    }
}
function removeAccents (str) {
    str = str.replace('á', 'a')
    str = str.replace('é', 'e')
    str = str.replace('í', 'i')
    str = str.replace('ó', 'o')
    str = str.replace('ú', 'u')
    return str
}
function generateNlu (intentions) {
    let str = ''
    intentions.forEach((intent, i) => {
        if (intent.ejemplos) {
            str = str + '## intent:' + intent.text + '\n'
            intent.ejemplos.forEach(ej => {
                str = str + '- ' + ej.text // +'\n';
            })
            str = str + '\n\n'
        }
    })
    return str
}
function generateAnswers (items) {
    let str = ''
    items.forEach((intent, i) => {
        str = str + intent.text + ','
        if (intent.respuesta) {
            str = str + '"' + intent.respuesta.text + '"\n'
        } else {
            console.log('ERROR matching answers: ', intent.text)
        }
    })
    return str
}
function generateIntentions (intsKBS, intsBM) {
    const namesBM = intsKBS.map(x => {
        return {
            type: 'bm',
            name: x.text,
            ejs: x.ejemplos.length,
            rta: x.respuesta ? 1 : 0
        }
    })
    const namesKBS = intsBM.map(x => {
        return {
            type: 'kbs',
            name: x.text,
            ejs: x.ejemplos.length,
            rta: x.respuesta ? 1 : 0
        }
    })

    const intNames = namesBM.concat(namesKBS)
    let str = 'tipo,intencion,ejs,rta\n'
    intNames.forEach(prop => {
        str =
            str +
            prop.type +
            ',' +
            prop.name +
            ',' +
            prop.ejs +
            ',' +
            prop.rta +
            '\n'
    })
    return str
}
function download (str, filename, mime) {
    const a = document.createElement('a')
    a.href = `data:text/${mime};charset=utf-8,` + encodeURIComponent(str)
    a.download = filename
    a.click()
    a.remove()
}
async function getIntentions ({ commit }) {
    try {
        Loading.show({
            spinner: QSpinnerGears,
            spinnerColor: 'white',
            spinnerSize: 140,
            backgroundColor: 'gray',
            message: 'cargando...',
            messageColor: 'black'
        })
        // const raw = await axios({
        //     method: 'GET',
        //     url: 'http://localhost:9000/api/getIntentions',
        //     headers: {
        //         'content-type': 'application/json',
        //         'Access-Control-Allow-Origin': '*',
        //         'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS'
        //     },
        //     params: {}
        // })
        const raw = await apiSrv('GET','/bot/getIntentions', {})
        const ints = raw.data
        console.log('ints:', ints)
        commit('setSelIntention', ints[0])
        commit('setIntentionsKBS', ints)
        return ints
    } catch (error) {
        console.log('error sendClient: ', error)
        return false
    } finally {
        Loading.hide()
    }
}
